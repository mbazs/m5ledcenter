#ifndef _M5LEDCENTER_STORAGE_H_
#define _M5LEDCENTER_STORAGE_H_

#include "config.h"
#include <FreeRTOS.h>
#include <freertos/timers.h>
#include <Arduino.h>
#include <EEPROM.h>

template <class T>
class EepromStorage
{
    static const int DATA_SIZE = sizeof(T)
#ifdef EEPROM_CRC
                                 + 1
#endif
        ;

public:
    typedef struct
    {
        unsigned char data[DATA_SIZE];
        EEPROMClass *eeprom;
    } storage_put_data_t;

    EepromStorage(const char *name);
    void load(T &data);
    void save(T &data);
    bool is_ok()
    {
        return _init_ok && _header_or_crc_ok;
    }
    static void storage_timer_cb(TimerHandle_t handle);
    ~EepromStorage();

private:
    const char *_name;
    EEPROMClass _eeprom;
    TimerHandle_t _timer_handle;
    storage_put_data_t _spd;
    bool _init_ok, _header_or_crc_ok;
};

template <class T>
void EepromStorage<T>::storage_timer_cb(TimerHandle_t handle)
{
    storage_put_data_t *spd = reinterpret_cast<storage_put_data_t *>(pvTimerGetTimerID(handle));
    spd->eeprom->writeBytes(0, spd->data, DATA_SIZE);
    spd->eeprom->commit();
#ifdef DEBMSG2
    Serial.printf("eeprom data written physically: %d bytes\n", DATA_SIZE);
#endif
}

template <class T>
EepromStorage<T>::EepromStorage(const char *name)
    : _name(name),
      _eeprom(name, DATA_SIZE),
      _init_ok(false),
      _header_or_crc_ok(false)
{
    _init_ok = _eeprom.begin(_eeprom.length());
    if (_init_ok)
    {
        _timer_handle = xTimerCreate(_name, pdMS_TO_TICKS(EEPROM_SAVE_PATIENCE_MS), pdFALSE, nullptr, storage_timer_cb);
    }
}

template <class T>
void EepromStorage<T>::load(T &data)
{
#ifndef EEPROM_CRC
    if (_init_ok && _eeprom.readByte(0) == 0x55 && _eeprom.readByte(1) == 0xaa)
    {
        _header_or_crc_ok = true;
        _eeprom.get(0, data);
#endif
#ifdef EEPROM_CRC
        _eeprom.readBytes(0, _spd.data, DATA_SIZE);
        unsigned char crc = 0;
        for (int i = 0; i < DATA_SIZE; ++i)
        {
            crc += _spd.data[i];
        }
        if (crc != 0)
        {
#ifdef DEBMSG2
            Serial.printf("EepromStorage: CRC failed (with 0x%.2X)\n", crc);
#endif
            _header_or_crc_ok = false;
            data = T();
        }
        else
        {
            _header_or_crc_ok = true;
            data = *reinterpret_cast<T *>(_spd.data + 1);
#ifdef DEBMSG2
            Serial.printf("EepromStorage: loaded %d bytes, CRC OK\n", DATA_SIZE);
#endif
        }
#endif
#ifndef EEPROM_CRC
    }
#endif
}

template <class T>
void EepromStorage<T>::save(T &data)
{
    if (_init_ok)
    {
#ifdef EEPROM_CRC
        unsigned char crc = 0;
        unsigned char *const ptr = reinterpret_cast<unsigned char *>(&data);
        for (int i = 0; i < DATA_SIZE - 1; ++i)
        {
            crc += ptr[i];
        }
        _spd.data[0] = -crc;
        memcpy(_spd.data + 1, &data, DATA_SIZE - 1);
#ifdef DEBMSG2
        Serial.printf("EepromStorage: saved %d bytes to buffer with CRC byte: 0x%.2X\n", DATA_SIZE, crc);
#endif
#else
        _spd.data = data;
#ifdef DEBMSG2
        Serial.printf("EepromStorage: saved %d bytes without CRC\n", DATA_SIZE);
#endif
#endif
        _spd.eeprom = &_eeprom;
        vTimerSetTimerID(_timer_handle, &_spd);
        xTimerReset(_timer_handle, pdMS_TO_TICKS(EEPROM_SAVE_PATIENCE_MS));
    }
}

template <class T>
EepromStorage<T>::~EepromStorage()
{
    if (_init_ok)
    {
        xTimerDelete(_timer_handle, 0);
    }
}

#endif
