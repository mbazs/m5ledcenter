#ifndef _M5LEDCENTER_COMMON_H_
#define _M5LEDCENTER_COMMON_H_

#include "config.h"
#include <Arduino.h>
#include <FastLED.h>
#include <EEPROM.h>

// FreeRTOS
#define NTP_CLOCK_SIZE 9
extern TaskHandle_t task_log_handle;
extern TaskHandle_t task_main_handle;
extern TaskHandle_t task_wifi_handle;
extern TaskHandle_t task_ntp_handle;
extern TaskHandle_t task_leds_handle;
extern TaskHandle_t task_webserver_handle;
extern EventGroupHandle_t eg_wifi_handle;
extern QueueHandle_t queue_ntp_handle;
extern QueueHandle_t queue_leds_handle;

// task_wifi
enum EGWifiStatus
{
    wifi_connected_flag = 0x01,
    wifi_disconnected_flag = 0x02,
    task_ntp_wifi_notify_flag = 0x04,
};
const EventBits_t group_wifi_connected = wifi_connected_flag | task_ntp_wifi_notify_flag;
const EventBits_t group_wifi_disconnected = wifi_disconnected_flag | task_ntp_wifi_notify_flag;

// other
extern volatile bool wifi_connected;
extern volatile int uptime;
extern volatile int last_webaccess;

// color RC commands and RGB codes
enum ColorCmd
{
    colorcmd_red = 0xF720DF,
    colorcmd_green = 0xF7A05F,
    colorcmd_blue = 0xF7609F,
    colorcmd_white = 0xF7E01F,
    colorcmd_orange = 0xF710EF,
    colorcmd_lightgreen = 0xF7906F,
    colorcmd_lightblue = 0xF750AF,
    colorcmd_flash = 0xF7D02F,
    colorcmd_lightorange = 0xF730CF,
    colorcmd_lightcyan = 0xF7B04F,
    colorcmd_purple = 0xF7708F,
    colorcmd_strobe = 0xF7F00F,
    colorcmd_lighterorange = 0xF708F7,
    colorcmd_cyan = 0xF78877,
    colorcmd_lightpurple = 0xF748B7,
    colorcmd_fade = 0xF7C837,
    colorcmd_yellow = 0xF728D7,
    colorcmd_darkercyan = 0xF7A857,
    colorcmd_pink = 0xF76897,
    colorcmd_smooth = 0xF7E817,
};

enum ColorRGB
{
    ledcolor_red = 0xFF0000,
    ledcolor_green = 0x00FF00,
    ledcolor_blue = 0x0000FF,
    ledcolor_white = 0xFFFFEE,
    ledcolor_orange = 0xFF8000,
    ledcolor_lightgreen = 0x33FF33,
    ledcolor_lightblue = 0x0080FF,
    ledcolor_lightorange = 0xFF9933,
    ledcolor_lightcyan = 0x00FFFF,
    ledcolor_purple = 0x990099,
    ledcolor_lighterorange = 0xFFB266,
    ledcolor_cyan = 0x00CCCC,
    ledcolor_lightpurple = 0xCC00CC,
    ledcolor_yellow = 0xFFFF00,
    ledcolor_darkercyan = 0x009999,
    ledcolor_pink = 0xFF00FF,
};

enum LedEffect
{
    ledeffect_flash = 0,
    //ledeffect_strobe = 1,
    //ledeffect_fade = 2,
    ledeffect_smooth = 1,
};

typedef struct
{
    ColorCmd cmd;
    ColorRGB rgb;
} ColorCmdToRGB;

typedef struct
{
    ColorCmd cmd;
    LedEffect effect;
} ColorCmdToEffect;

#define LEDCOLOR_COUNT 16
#define LEDEFFECT_COUNT 2
extern const ColorCmdToRGB colorCmdToRGBLookup[LEDCOLOR_COUNT];
extern const ColorCmdToEffect colorCmdToEffectLookup[LEDEFFECT_COUNT];
extern const ColorRGB ledcolor_array[LEDCOLOR_COUNT];
extern const LedEffect ledeffect_array[LEDEFFECT_COUNT];

// task_leds
enum LedsTaskCmd
{
    task_leds_cmd_set_subtask,
    task_leds_cmd_set_color,
    task_leds_cmd_set_effect,
    task_leds_cmd_brightness_inc,
    task_leds_cmd_brightness_dec,
};

enum LedsTaskSubtask
{
    task_leds_subtask_motion,
    task_leds_subtask_clock,
};

typedef struct
{
#ifndef EEPROM_CRC
    unsigned char hdr[2] = {0x55, 0xaa};
#endif
    TickType_t xDelay;
    CRGB *led_array;
    uint8_t brightness = DEFAULT_BRIGHTNESS;
    ColorRGB color = ledcolor_red;
    LedEffect effect = ledeffect_smooth;
} LedsTaskCtx;

typedef struct
{
    LedsTaskCmd cmd;
    union
    {
        LedsTaskSubtask subtask;
        ColorRGB color;
        LedEffect effect;
    };
} LedsTaskCmdParams;

#define DATE_STR_LENGTH 11
extern char *get_date_str(char *buf);
#define TIME_STR_LENGTH 9
extern char *get_time_str(char *buf);
#define DATETIME_STR_LENGTH 20
extern char *get_datetime_str(char *buf);

#endif
