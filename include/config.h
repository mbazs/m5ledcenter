#ifndef _M5LEDCENTER_CONFIG_H_
#define _M5LEDCENTER_CONFIG_H_
#include <FreeRTOS.h>

// stack, logging
#define DEFAULT_STACKSIZE 4096
#define DEBMSG
#define DEBMSG2
//#define USE_LED

// FreeRTOS
#define DEFAULT_DELAY_MS 100
#define DEFAULT_DELAY_TICK pdMS_TO_TICKS(DEFAULT_DELAY_MS)
#define TASK_MAIN_PRIO 12
#define TASK_LOG_PRIO 5
#define TASK_WIFI_PRIO 10
#define TASK_LEDS_PRIO 15
#define TASK_WEBSERVER_PRIO 10
// task priority checks
#if TASK_MAIN_PRIO > configMAX_PRIORITIES || \
    TASK_LOG_PRIO > configMAX_PRIORITIES ||  \
    TASK_WIFI_PRIO > configMAX_PRIORITIES || \
    TASK_LEDS_PRIO > configMAX_PRIORITIES || \
    TASK_WEBSERVER_PRIO > configMAX_PRIORITIES
#error please check priorities
#endif

// wifi
#define WIFI_SSID "BaRo"
#define WIFI_PWD "Hupakolas6789"
#define WIFI_IP IPAddress(192, 168, 0, 222)
#define WIFI_GATEWAY IPAddress(192, 168, 0, 1)
#define WIFI_MASK IPAddress(255, 255, 255, 0)
#define WIFI_DNS IPAddress(8, 8, 8, 8)
#define WIFI_RECONNECT_DELAY_MS 5000
#define WIFI_RECONNECT_LIMIT 5

// infrared RC
#define IR_RECV_PORT G36

// leds
#define NUM_LEDS 30
#define FASTLED_CHIPSET WS2812B
#define FASTLED_COLOR_ORDER GRB
#define FASTLED_OUT_PORT G0
#define STRIPE_ON_AFTER_MOTION_SECS 25
#define STRIPE_FADE_SECS 3
#define DEFAULT_BRIGHTNESS 50
#define MAX_BRIGHTNESS 255
#define MIN_BRIGHTNESS 10
#define BRIGHTNESS_STEP 10

// leds subtask: clock
const int CLOCK_MIDDLE_LED = NUM_LEDS / 2;
const int CLOCK_START_LED = 0;

// motion
#define MOTION_IN_PORT G26

// eeprom
#define EEPROM_SAVE_PATIENCE_MS 30000
#define EEPROM_CRC

// NTP
#define NTP_TIMEZONE "CET-1CEST,M3.5.0,M10.5.0/3"
#define NTP_SERVER "pool.ntp.org"

#endif
