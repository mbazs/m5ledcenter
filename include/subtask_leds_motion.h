#ifndef _M5LEDCENTER_SUBTASK_LEDS_MOTION_
#define _M5LEDCENTER_SUBTASK_LEDS_MOTION_

void subtask_leds_motion_init(const LedsTaskCtx &params);
void subtask_leds_motion(const LedsTaskCtx &params);

#endif
