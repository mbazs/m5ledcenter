#ifndef _M5LEDCENTER_SUBTASK_LEDS_CLOCK_
#define _M5LEDCENTER_SUBTASK_LEDS_CLOCK_

void subtask_leds_clock_init(const LedsTaskCtx &params);
void subtask_leds_clock(const LedsTaskCtx &params);

#endif
