#include "config.h"
#include "common.h"
#include "task_log.h"
#include "task_wifi.h"
#include "task_leds.h"
//#include "task_webserver.h"
#include <Arduino.h>
#include <M5StickC.h>
#include <M5Display.h>
#include <IRrecv.h>

void task_main(void *pvParameters);

void setup()
{
  M5.begin();
  pinMode(MOTION_IN_PORT, INPUT);
  pinMode(FASTLED_OUT_PORT, OUTPUT);
#ifdef USE_LED
  pinMode(M5_LED, OUTPUT);
  digitalWrite(M5_LED, HIGH);
#endif
  // FreeRTOS init
  eg_wifi_handle = xEventGroupCreate();
  queue_ntp_handle = xQueueCreate(1, NTP_CLOCK_SIZE);
  queue_leds_handle = xQueueCreate(1, sizeof(LedsTaskCmdParams));

  // spawning tasks
  xTaskCreate(task_main, "task_main", DEFAULT_STACKSIZE, nullptr, TASK_MAIN_PRIO, &task_main_handle);
  xTaskCreate(task_log, "task_log", DEFAULT_STACKSIZE, nullptr, TASK_LOG_PRIO, &task_log_handle);
  xTaskCreate(task_wifi, "task_wifi", DEFAULT_STACKSIZE, nullptr, TASK_WIFI_PRIO, &task_wifi_handle);
  xTaskCreate(task_leds, "task_leds", DEFAULT_STACKSIZE, nullptr, TASK_LEDS_PRIO, &task_leds_handle);
  //xTaskCreate(task_webserver, "task_webserver", DEFAULT_STACKSIZE, nullptr, TASK_WEBSERVER_PRIO, &ntpTaskHandle);
  //vTaskStartScheduler();
}

void loop()
{
}
