#include "config.h"
#include "common.h"
#include "subtask_leds_clock.h"
#include <Arduino.h>
#include <FastLED.h>

CRGB clock_bit_to_color(const bool bit)
{
    return bit ? CRGB(0xff, 0x00, 0x00) : CRGB(0x00, 0x00, 0xff);
}

void fill_clock(CRGB **leds, unsigned char digit)
{
    for (int i = 0; i < 4; ++i)
    {
        *((*leds)++) = clock_bit_to_color(digit & 0x08);
        digit <<= 1;
    }
    (*leds)++;
}

void subtask_leds_clock_init(const LedsTaskCtx &ctx)
{
}

void subtask_leds_clock(const LedsTaskCtx &ctx)
{
    char ts[TIME_STR_LENGTH];
    get_time_str(ts);
    FastLED.clear();
    CRGB *p = ctx.led_array + CLOCK_START_LED;
    fill_clock(&p, ts[0] - '0');
    fill_clock(&p, ts[1] - '0');
    fill_clock(&p, ts[3] - '0');
    fill_clock(&p, ts[4] - '0');
    fill_clock(&p, ts[6] - '0');
    fill_clock(&p, ts[7] - '0');
    FastLED.show();
    vTaskDelay(ctx.xDelay);
}
