#include "config.h"
#include "common.h"
#include "task_leds.h"
#include "subtask_leds_motion.h"
#include "subtask_leds_clock.h"
#include "storage.h"
#include <Arduino.h>
#include <FastLED.h>

void process_cmd(const LedsTaskCmdParams &cmd_params, EepromStorage<LedsTaskCtx> &ees);
void clear_leds();

static CRGB led_array[NUM_LEDS];
static LedsTaskCtx ctx;
static LedsTaskSubtask current_subtask = task_leds_subtask_motion;

void task_leds(void *pvParameters)
{
    EepromStorage<LedsTaskCtx> ees("leds");
    const TickType_t xDelay = pdMS_TO_TICKS(100);
    LedsTaskCmdParams cmd_params;

    ees.load(ctx);
    FastLED.addLeds<FASTLED_CHIPSET, FASTLED_OUT_PORT, FASTLED_COLOR_ORDER>(led_array, NUM_LEDS).setCorrection(TypicalLEDStrip);
    FastLED.setBrightness(ctx.brightness);
    ctx.xDelay = xDelay;
    ctx.led_array = led_array;
    subtask_leds_motion_init(ctx);
    subtask_leds_clock_init(ctx);
    for (;;)
    {
        if (xQueueReceive(queue_leds_handle, &cmd_params, 0) == pdTRUE)
        {
            process_cmd(cmd_params, ees);
        }
        switch (current_subtask)
        {
        case task_leds_subtask_motion:
            subtask_leds_motion(ctx);
            break;
        case task_leds_subtask_clock:
            subtask_leds_clock(ctx);
            break;
        }
    }
}

void process_cmd(const LedsTaskCmdParams &cmd_params, EepromStorage<LedsTaskCtx> &ees)
{
    bool save = true;
    switch (cmd_params.cmd)
    {
    case task_leds_cmd_set_subtask:
        current_subtask = cmd_params.subtask;
        clear_leds();
        save = false;
        break;
    case task_leds_cmd_set_color:
        ctx.color = cmd_params.color;
        break;
    case task_leds_cmd_set_effect:
        ctx.effect = cmd_params.effect;
        break;
    case task_leds_cmd_brightness_inc:
        if (ctx.brightness + BRIGHTNESS_STEP < MAX_BRIGHTNESS)
        {
            ctx.brightness += BRIGHTNESS_STEP;
        }
        else
        {
            ctx.brightness = MAX_BRIGHTNESS;
        }
        break;
    case task_leds_cmd_brightness_dec:
        if (ctx.brightness - BRIGHTNESS_STEP > MIN_BRIGHTNESS)
        {
            ctx.brightness -= BRIGHTNESS_STEP;
        }
        else
        {
            ctx.brightness = MIN_BRIGHTNESS;
        }
        break;
    default:
        break;
    }
    FastLED.setBrightness(ctx.brightness);
    if (save)
    {
        ees.save(ctx);
    }
}

void clear_leds()
{
    fill_solid(ctx.led_array, NUM_LEDS, CRGB(0, 0, 0));
    FastLED.show();
}
