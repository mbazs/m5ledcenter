#include "config.h"
#include "common.h"
#include "task_main.h"
#include <Arduino.h>
#include <M5StickC.h>
#include <M5Display.h>
#include <IRrecv.h>
#include <ESPPerfectTime.h>

static TFT_eSprite tftSprite = TFT_eSprite(&M5.Lcd);
static IRrecv ir_recv(IR_RECV_PORT);
static LedsTaskSubtask current_leds_subtask = task_leds_subtask_motion;
static int current_ledcolor_index = 0;
static int current_effect_index = 1;

void process_rc(uint64_t code);

void task_main(void *pvParameters)
{
    uint64_t x64;

    const TickType_t xDelay = pdMS_TO_TICKS(100);

    // hw init
    M5.Rtc.begin();
    M5.Lcd.begin();
    M5.Lcd.setRotation(3);
    M5.Lcd.invertDisplay(true);
    M5.Axp.begin();
    M5.Axp.ScreenBreath(7);
    tftSprite.createSprite(160, 80);
    tftSprite.setRotation(3);

    // IR init
    ir_recv.enableIRIn();

    // NTP
    pftime::configTzTime(PSTR(NTP_TIMEZONE), NTP_SERVER);

    decode_results ir_recv_results;
    for (;;)
    {
        if (ir_recv.decode(&ir_recv_results))
        {
            process_rc(ir_recv_results.value);
            ir_recv.resume();
        }
        if (M5.BtnA.isPressed())
        {
            esp_restart();
        }
        tftSprite.fillSprite(BLACK);
        tftSprite.setCursor(0, 0, 1);
        char ts[DATETIME_STR_LENGTH];
        tftSprite.printf("%s\n\n", get_datetime_str(ts));
        tftSprite.printf("Uptime (.1s): %d\n", uptime++);
        tftSprite.printf("Wifi        : %s\n", wifi_connected ? "yes" : "no");
        tftSprite.printf("Access      : ");
        if (last_webaccess >= 0)
        {
            tftSprite.printf("%d\n", last_webaccess);
        }
        else
        {
            tftSprite.println();
        }
        tftSprite.pushSprite(0, 0);
        M5.update();
        vTaskDelay(xDelay);
    }
}

void process_rc(uint64_t code)
{
    LedsTaskCmdParams task_led_cmd_params;

    // color command
    for (int i = 0; i < sizeof(colorCmdToRGBLookup) / sizeof(ColorCmdToRGB); ++i)
    {
        if (colorCmdToRGBLookup[i].cmd == code)
        {
            task_led_cmd_params.cmd = task_leds_cmd_set_color;
            task_led_cmd_params.color = colorCmdToRGBLookup[i].rgb;
            xQueueOverwrite(queue_leds_handle, &task_led_cmd_params);
        }
    }

    // effect command
    for (int i = 0; i < sizeof(colorCmdToEffectLookup) / sizeof(ColorCmdToEffect); ++i)
    {
        if (colorCmdToEffectLookup[i].cmd == code)
        {
            task_led_cmd_params.cmd = task_leds_cmd_set_effect;
            task_led_cmd_params.effect = colorCmdToEffectLookup[i].effect;
            xQueueOverwrite(queue_leds_handle, &task_led_cmd_params);
        }
    }

    switch (code)
    {
    case 0xFFE21D: // CH+
        if (++current_ledcolor_index >= LEDCOLOR_COUNT)
        {
            current_ledcolor_index = 0;
        }
        task_led_cmd_params.cmd = task_leds_cmd_set_color;
        task_led_cmd_params.color = ledcolor_array[current_ledcolor_index];
        xQueueOverwrite(queue_leds_handle, &task_led_cmd_params);
        break;
    case 0xFFA25D: // CH-
        if (--current_ledcolor_index < 0)
        {
            current_ledcolor_index = LEDCOLOR_COUNT - 1;
        }
        task_led_cmd_params.cmd = task_leds_cmd_set_color;
        task_led_cmd_params.color = ledcolor_array[current_ledcolor_index];
        xQueueOverwrite(queue_leds_handle, &task_led_cmd_params);
        break;
    case 0xFF629D: // CH
        switch (current_leds_subtask)
        {
        case task_leds_subtask_motion:
            current_leds_subtask = task_leds_subtask_clock;
            break;
        case task_leds_subtask_clock:
            current_leds_subtask = task_leds_subtask_motion;
            break;
        }
        task_led_cmd_params.cmd = task_leds_cmd_set_subtask;
        task_led_cmd_params.subtask = current_leds_subtask;
        xQueueOverwrite(queue_leds_handle, &task_led_cmd_params);
        break;
    case 0xFFE01F: // vol-
    case 0xF7807F: // light down
        task_led_cmd_params.cmd = task_leds_cmd_brightness_dec;
        xQueueOverwrite(queue_leds_handle, &task_led_cmd_params);
        break;
    case 0xFFA857: // vol+
    case 0xF700FF: // light up
        task_led_cmd_params.cmd = task_leds_cmd_brightness_inc;
        xQueueOverwrite(queue_leds_handle, &task_led_cmd_params);
        break;
    case 0xFF906F: // eq
        if (++current_effect_index >= LEDEFFECT_COUNT)
        {
            current_effect_index = 0;
        }
        task_led_cmd_params.cmd = task_leds_cmd_set_effect;
        task_led_cmd_params.effect = ledeffect_array[current_effect_index];
        xQueueOverwrite(queue_leds_handle, &task_led_cmd_params);
        break;
    case 0xFF6897:
        M5.Axp.PowerOff();
        break;
    case 0xFF52AD:
        esp_restart();
        break;
    }
}
