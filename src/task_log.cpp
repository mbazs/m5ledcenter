#include "config.h"
#include "common.h"
#include "task_log.h"
#include <Arduino.h>

void task_log(void *pvParameters)
{
    const TickType_t xDelay = pdMS_TO_TICKS(100);
    int counter = 0;

    for (;;)
    {
        if (counter++ >= 50)
        {
            counter = 0;
#ifdef DEBMSG
            Serial.printf("connected to wifi  : %s\n", wifi_connected ? "yes" : "no");
            Serial.printf("task_main watermark: %d\n", uxTaskGetStackHighWaterMark(task_main_handle));
            Serial.printf("task_log watermark : %d\n", uxTaskGetStackHighWaterMark(task_log_handle));
            Serial.printf("task_wifi watermark: %d\n", uxTaskGetStackHighWaterMark(task_wifi_handle));
            Serial.printf("task_ntp  watermark: %d\n", uxTaskGetStackHighWaterMark(task_ntp_handle));
            Serial.printf("task_leds watermark: %d\n", uxTaskGetStackHighWaterMark(task_leds_handle));
            Serial.printf("heap size          : %d\n", ESP.getHeapSize());
            Serial.printf("heap free          : %d\n", ESP.getFreeHeap());
            Serial.println("-----------------------------------------------------");
#endif
        }
        vTaskDelay(xDelay);
    }
}
