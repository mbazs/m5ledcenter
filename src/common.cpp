#include "common.h"
#include <M5StickC.h>
#include <ESPPerfectTime.h>

TaskHandle_t task_log_handle = nullptr;
TaskHandle_t task_main_handle = nullptr;
TaskHandle_t task_wifi_handle = nullptr;
TaskHandle_t task_ntp_handle = nullptr;
TaskHandle_t task_leds_handle = nullptr;
TaskHandle_t task_webserver_handle = nullptr;
EventGroupHandle_t eg_wifi_handle = nullptr;
QueueHandle_t queue_ntp_handle = nullptr;
QueueHandle_t queue_leds_handle = nullptr;

volatile bool wifi_connected = false;
volatile int uptime = 0;
volatile int last_webaccess = -1;

const ColorRGB ledcolor_array[] = {
    ledcolor_red,
    ledcolor_green,
    ledcolor_blue,
    ledcolor_white,
    ledcolor_orange,
    ledcolor_lightgreen,
    ledcolor_lightblue,
    ledcolor_lightorange,
    ledcolor_lightcyan,
    ledcolor_purple,
    ledcolor_lighterorange,
    ledcolor_cyan,
    ledcolor_lightpurple,
    ledcolor_yellow,
    ledcolor_darkercyan,
    ledcolor_pink,
};

const LedEffect ledeffect_array[] = {
    ledeffect_flash,
    //ledeffect_strobe,
    //ledeffect_fade,
    ledeffect_smooth,
};

const ColorCmdToRGB colorCmdToRGBLookup[] = {
    {cmd : colorcmd_red, rgb : ledcolor_red},
    {cmd : colorcmd_green, rgb : ledcolor_green},
    {cmd : colorcmd_blue, rgb : ledcolor_blue},
    {cmd : colorcmd_white, rgb : ledcolor_white},
    {cmd : colorcmd_orange, rgb : ledcolor_orange},
    {cmd : colorcmd_lightgreen, rgb : ledcolor_lightgreen},
    {cmd : colorcmd_lightblue, rgb : ledcolor_lightblue},
    {cmd : colorcmd_lightorange, rgb : ledcolor_lightorange},
    {cmd : colorcmd_lightcyan, rgb : ledcolor_lightcyan},
    {cmd : colorcmd_purple, rgb : ledcolor_purple},
    {cmd : colorcmd_lighterorange, rgb : ledcolor_lighterorange},
    {cmd : colorcmd_cyan, rgb : ledcolor_cyan},
    {cmd : colorcmd_lightpurple, rgb : ledcolor_lightpurple},
    {cmd : colorcmd_yellow, rgb : ledcolor_yellow},
    {cmd : colorcmd_darkercyan, rgb : ledcolor_darkercyan},
    {cmd : colorcmd_pink, rgb : ledcolor_pink},
};

const ColorCmdToEffect colorCmdToEffectLookup[] = {
    {cmd : colorcmd_flash, effect : ledeffect_flash},
    //{cmd : colorcmd_strobe, effect : ledeffect_strobe},
    //{cmd : colorcmd_fade, effect : ledeffect_fade},
    {cmd : colorcmd_smooth, effect : ledeffect_smooth}};

char *get_date_str(char *buf)
{
    char ts[DATETIME_STR_LENGTH];
    strncpy(buf, get_datetime_str(ts), DATE_STR_LENGTH);
    buf[DATE_STR_LENGTH] = 0;
    return buf;
}

char *get_time_str(char *buf)
{
    char ts[DATETIME_STR_LENGTH];
    strncpy(buf, get_datetime_str(ts) + 11, TIME_STR_LENGTH);
    buf[TIME_STR_LENGTH] = 0;
    return buf;
}

char *get_datetime_str(char *buf)
{
    int year, month, day, hour, min, sec;
    char type = 0;
    struct tm *tm = pftime::localtime(nullptr);
    bool ntp = false;

    if (tm->tm_year > 100)
    {
        //NTP acquired
        ntp = true;
        year = tm->tm_year + 1900;
        month = tm->tm_mon + 1;
        day = tm->tm_mday;
        type = 'N';
        hour = tm->tm_hour;
        min = tm->tm_min;
        sec = tm->tm_sec;
    }
    else
    {
        // no NTP
        year = M5.Rtc.Year;
        month = M5.Rtc.Month;
        day = M5.Rtc.Day;
        type = 'R';
        hour = M5.Rtc.Hour;
        min = M5.Rtc.Minute;
        sec = M5.Rtc.Second;
    }
    sprintf(buf, "%04d-%02d-%02d%c%02d:%02d:%02d", year, month, day, type, hour, min, sec);
    if (ntp)
    {
        RTC_DateTypeDef dtd;
        dtd.Year = year;
        dtd.Month = month;
        dtd.Date = day;
        RTC_TimeTypeDef ttd;
        ttd.Hours = hour;
        ttd.Minutes = min;
        ttd.Seconds = sec;
        M5.Rtc.SetData(&dtd);
        M5.Rtc.SetTime(&ttd);
    }
    return buf;
}
