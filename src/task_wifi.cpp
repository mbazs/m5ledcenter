#include "config.h"
#include "common.h"
#include "task_wifi.h"
#include <Arduino.h>
#include <WiFi.h>
#include <M5StickC.h>

void task_wifi(void *pvParameters)
{
        const TickType_t xDelay = pdMS_TO_TICKS(WIFI_RECONNECT_DELAY_MS);

        for (;;)
        {
                wifi_connected = false;
                WiFi.config(WIFI_IP, WIFI_GATEWAY, WIFI_MASK, WIFI_DNS);
                WiFi.begin(WIFI_SSID, WIFI_PWD);
#ifdef DEBMSG2
                Serial.println("connecting to wifi ...");
#endif
                int reconnect = 0;
                if (WiFi.status() != WL_CONNECTED)
                {
                        do
                        {
#ifdef DEBMSG2
                                Serial.println("waiting for wifi ...");
#endif
                                vTaskDelay(xDelay);
                                ++reconnect;
                        } while (WiFi.status() != WL_CONNECTED && reconnect < WIFI_RECONNECT_LIMIT);
                        if (reconnect == WIFI_RECONNECT_LIMIT)
                        {
#ifdef DEBMSG2
                                Serial.println("reconnecting wifi ...");
#endif
                                continue;
                        }
                }

                // connected
                wifi_connected = true;
#ifdef USE_LED
                digitalWrite(M5_LED, LOW);
#endif
#ifdef DEBMSG2
                Serial.println("connected to wifi, waiting for notifying all tasks");
#endif
                //xEventGroupSync(eg_wifi_handle, wifi_connected_flag, group_wifi_connected, portMAX_DELAY);
#ifdef DEBMSG2
                Serial.println("connected to wifi, notified all tasks");
#endif
                while (WiFi.status() == WL_CONNECTED && !M5.BtnB.isPressed())
                {
                        vTaskDelay(xDelay);
                }
                vTaskDelay(xDelay);

                // disconnected
#ifdef USE_LED
                digitalWrite(M5_LED, HIGH);
#endif
#ifdef DEBMSG2
                Serial.println("disconnected from wifi, waiting for notifying all tasks");
#endif
                //xEventGroupSync(eg_wifi_handle, wifi_disconnected_flag, group_wifi_disconnected, portMAX_DELAY);
#ifdef DEBMSG2
                Serial.println("disconnected from wifi, notified all tasks");
#endif
        }
}
