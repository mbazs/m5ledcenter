#include "config.h"
#include "common.h"
#include "subtask_leds_motion.h"
#include <Arduino.h>
#include <FastLED.h>

void fill_leds(const LedsTaskCtx &ctx);

enum CurrentState
{
    motion_off,
    motion_light,
    motion_fade,
};
static CurrentState current_state;
static unsigned long timer = 0, last_motion = 0;

bool is_motion()
{
    return /*M5.BtnB.isPressed() || */ digitalRead(MOTION_IN_PORT);
}

void shutdown_leds(const LedsTaskCtx &ctx)
{
    current_state = motion_off;
    fill_solid(ctx.led_array, NUM_LEDS, CRGB(0, 0, 0));
    FastLED.show();
}

void subtask_leds_motion_init(const LedsTaskCtx &ctx)
{
    shutdown_leds(ctx);
}

void subtask_leds_motion(const LedsTaskCtx &ctx)
{
    switch (current_state)
    {
    case motion_off:
        timer = 0;
        last_motion = 0;
        if (is_motion())
        {
            current_state = motion_light;
        }
        break;
    case motion_light:
        fill_leds(ctx);
        if (is_motion())
        {
            last_motion = timer;
        }
        else if (timer > last_motion + STRIPE_ON_AFTER_MOTION_SECS * 10)
        {
            timer = last_motion = 0;
            current_state = motion_fade;
            break;
        }
        break;
    case motion_fade:
        fadeToBlackBy(ctx.led_array, NUM_LEDS, 30);
        FastLED.show();
        if (is_motion())
        {
            last_motion = timer;
            current_state = motion_light;
        }
        else if (timer > last_motion + STRIPE_FADE_SECS * 10)
        {
            shutdown_leds(ctx);
        }
        break;
    }
    ++timer;
    vTaskDelay(ctx.xDelay);
}

void fill_leds(const LedsTaskCtx &ctx)
{
    switch (ctx.effect)
    {
    case ledeffect_flash:
        fill_rainbow(ctx.led_array, NUM_LEDS, (timer * 4) & 0xff, 5);
        break;
    //case ledeffect_strobe:
    //    break;
    //case ledeffect_fade:
    //    break;
    case ledeffect_smooth:
        fill_solid(ctx.led_array, NUM_LEDS, CRGB(ctx.color));
        break;
    default:
        break;
    }
    FastLED.show();
}
